Django Discourse SSO package
============================

A well tested Django package that implements discourse SSO. 

How to integrate with discourse
-------------------------------

Add a ``discourse_django_sso.views.SSOProviderView`` to your urls 
and point set this url as ``sso_url`` in discourse settings. 

Example: 
    
    urlpatterns = [
        url(
            r'^sso/',
            views.SSOProviderView.as_view(
                sso_redirect=settings.DISCOURSE_SSO_REDIRECT,
                sso_secret=settings.DISCOURSE_SSO_KEY
            ),
            name="sso"
        ),
    ]


This view is configured by keyword arguments to ``as_view`` function, 
``sso_redirect`` and ``sso_secret`` are mandatory arguments. 

Currently this works for Django 1.10+ and Python 3.5+.  

Extra features: 

1. You can use this packages as SSO consumer, e.g. if you want to integrate 
   two django services together. See ``SSOClientView`` for details. 

This project has received funding from the European Union Horizon 2020 Research
and Innovation programme [under grant agreement no. 710577](https://cordis.europa.eu/project/rcn/203170_en.html). 

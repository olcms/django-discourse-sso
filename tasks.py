# coding=utf-8

from invoke import task


@task
def pep8(ctx):
  ctx.run("black --check discourse_django_sso discourse_django_sso_test")


@task
def lint(ctx):
  ctx.run("pylint discourse_django_sso discourse_django_sso_test -r n")


@task
def test(ctx):
  ctx.run("py.test -v --cov discourse_django_sso --cov-report=html --cov-report=term-missing discourse_django_sso_test")


@task(pre=[test, pep8, lint])
def check(ctx):
  pass




